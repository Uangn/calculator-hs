{-# LANGUAGE DeriveFoldable #-}
{-# LANGUAGE DeriveFunctor #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE OverloadedStrings #-}

module Main (main) where

import Tokenize (OperatorType (Div), Token (..), tokenize, typeToFunc, typeToOper, typeToPrec)

import Control.Arrow (Arrow (..))
import Control.Monad (MonadPlus (..), join, mfilter)
import Control.Monad.Trans.State (StateT (..), evalStateT, gets, modify)
import Data.Foldable (sequenceA_, traverse_)
import Data.List (uncons)
import Data.Maybe (fromMaybe)
import qualified Data.Text as T
import Language.Haskell.Interpreter

evala exp = runInterpreter $ setImports ["Prelude"] >> eval (T.unpack exp)

main :: IO ()
main = do
    let tests =
            [ "1+2*3+4-3*2*2/3+4+(2*3/3+2)"
            , "1+2+3+4"
            , "1-2-3-4"
            , "0/3"
            , "0/3+2*3-3*2*(7+2/4)"
            , "4/0"
            , "0/0"
            ]
        tokens = fromMaybe [] <$> (evalStateT tokenize <$> tests)
        parsed = runStateT parse <$> tokens
    print tests
    sequenceA_ $
        zipWith
            ( \t mp -> case mp of
                Just (p, []) -> do
                    print t
                    print (showStdExpr p)
                    (Right controlEval) <- evala t
                    let test = evalStdExpr p
                        control = read controlEval
                    print (control, test, control == test)
                _ -> print $ "failed: " <> t
            )
            tests
            parsed
    return ()

evalStdExpr :: StdExpr -> Double
evalStdExpr (Value x) = x
evalStdExpr (Expr l o r) = (typeToFunc o) (evalStdExpr l) (evalStdExpr r)

showStdExpr :: StdExpr -> String
showStdExpr (Value x) = show x
showStdExpr (Expr l o r) =
    join
        [ "("
        , (showStdExpr l)
        , (typeToOper o : "")
        , (showStdExpr r)
        , ")"
        ]

data Expr o a
    = Value a
    | Expr (Expr o a) o (Expr o a)
    deriving (Show, Functor, Foldable)

type StdExpr = Expr OperatorType Double

type TokenParser = StateT [Token] Maybe

parse :: TokenParser StdExpr
parse = do
    result <- part (-1 :: Int)
    isDone <- gets null
    if isDone
        then pure result
        else do
            operToken <- operator
            Expr result operToken <$> parse
  where
    part :: Int -> TokenParser StdExpr
    part prevPrec = do
        lhsToken <- token
        lhs <- case lhsToken of
            Number n -> pure $ Value n
            Paren ts -> case runStateT parse ts of
                Just (x, []) -> pure x -- not done
                x -> fail $ "failed parse idk why: parens " <> show x
            x -> fail $ "got [something] instead of [atom] " <> show x
        op prevPrec lhs

    op :: Int -> StdExpr -> TokenParser StdExpr
    op prevPrec lhs = do
        operToken <- token
        case operToken of
            EOF -> pure lhs
            Operator oper -> do
                let currPrec = typeToPrec oper
                if currPrec <= prevPrec
                    then modify (operToken :) >> pure lhs
                    else Expr lhs oper <$> part currPrec
            x -> fail $ "got an [atom] instead of [eof | operator] " <> show x

operator :: TokenParser OperatorType
operator = isOperator =<< token

isOperator :: (MonadPlus m) => Token -> m OperatorType
isOperator (Operator x) = pure x
isOperator _ = mzero

tokenIs :: (Token -> Bool) -> TokenParser Token
tokenIs p = mfilter p token

token :: TokenParser Token
token = StateT uncons
