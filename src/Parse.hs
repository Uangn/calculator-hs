{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE ImportQualifiedPost #-}
{-# LANGUAGE OverloadedStrings #-}

module Parse (
    Parser,
    TextParser,
    item,
    sat,
    char,
    digit,
    number,
    space,
    paren,
    double,
    between,
) where

import Control.Applicative (Alternative (..))
import Control.Monad (MonadPlus (..), mfilter, void)
import Control.Monad.Trans.State (StateT (..))
import Data.Char (isDigit, isSpace)
import Data.Text qualified as T

type Parser s = StateT s Maybe
type TextParser = Parser T.Text

between :: Parser s a -> Parser s c -> Parser s b -> Parser s b
between a c b = a *> b <* c

paren :: TextParser a -> TextParser a
paren x = char '(' *> x <* char ')'

double :: TextParser Double
double = read <$> number `mplus` liftA2 (:) (char '.') number

item :: TextParser Char
item = StateT T.uncons

sat :: (Char -> Bool) -> TextParser Char
sat p = mfilter p item

char :: Char -> TextParser Char
char c = sat (c ==)

digit :: TextParser Char
digit = sat isDigit

number :: TextParser String
number = some digit

space :: TextParser ()
space = void $ sat isSpace
