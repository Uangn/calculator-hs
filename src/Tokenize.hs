{-# LANGUAGE LambdaCase #-}

module Tokenize (
    tokenize,
    Token (..),
    OperatorType (..),
    typeToPrec,
    typeToOper,
    typeToFunc,
) where

import Parse (TextParser, double, item, paren, space)

import Control.Applicative (Alternative (..))
import Control.Monad (MonadPlus (..), void)

data Token
    = Number !Double
    | Operator !OperatorType
    | Paren [Token]
    | EOF
    deriving (Show, Eq)

data OperatorType
    = Plus
    | Minus
    | Mult
    | Div
    deriving (Show, Eq)

tokenize :: TextParser [Token]
tokenize = (`snoc` EOF) <$> (many lexeme)

snoc :: [a] -> a -> [a]
snoc xs x = foldr (:) [x] xs

lexeme :: TextParser Token
lexeme = do
    void $ many space
    l <- (Number <$> double) <|> (Paren <$> paren tokenize) <|> oper
    void $ many space
    pure l

--------------------
-- Operators

operToType :: (MonadPlus m) => Char -> m OperatorType
operToType = \case
    '+' -> pure Plus
    '-' -> pure Minus
    '*' -> pure Mult
    '/' -> pure Div
    _ -> mzero

typeToFunc :: (Fractional a) => OperatorType -> a -> a -> a
typeToFunc = \case
    Plus -> (+)
    Minus -> (-)
    Mult -> (*)
    Div -> (/)

typeToOper :: OperatorType -> Char
typeToOper = \case
    Plus -> '+'
    Minus -> '-'
    Mult -> '*'
    Div -> '/'

typeToPrec :: OperatorType -> Int
typeToPrec = \case
    Plus -> 1
    Minus -> 1
    Mult -> 2
    Div -> 2

--------------------

oper :: TextParser Token
oper =
    maybe mzero pure
        =<< fmap Operator . operToType
            <$> item
